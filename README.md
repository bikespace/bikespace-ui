## How to start

```js
npm install
npm run watch
```

Open the file dist/index.html

```sh
open dist/index.html
```

## How to build

```sh
docker build -t "bikespace-ui" .
docker run -d -p 0.0.0.0:80:80 bikespace-ui
```